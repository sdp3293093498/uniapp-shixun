import Vue from 'vue'
import App from './App'
import { Http } from '@anyup/uni-http'
const http = new Http()
const baseURl = 'http://localhost:8082/api/'
http.setBaseURL(baseURl)
Vue.config.productionTip = false
uni.$Http = http
App.mpType = 'app'

Vue.filter("formatDate", (date) => {
	const nDate = new Date(date)
	const nyear = nDate.getFullYear()
	const nmonth = (nDate.getMonth()+1).toString().padStart(2,0)
	const nday = nDate.getDate().toString().padStart(2,0)
	return nyear+'-'+nmonth+'-'+nday
})
const app = new Vue({
    ...App
})
app.$mount()